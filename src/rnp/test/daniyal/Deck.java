package rnp.test.daniyal;

import java.util.ArrayList;
import java.util.Random;

/**
 * This class provides a simple implementation of Deck of cards, with a function
 * to shuffle the cards and a function to deal the top-most card from the deck.
 * The dealt card has a string representation with rank and suit (e.g. A C
 * represents Ace of Clubs whereas 10 D represents 10 of Diamonds).
 * 
 * @author ali.daniyal
 * 
 */
public class Deck {
	private ArrayList<String> cards = new ArrayList<String>();

	// This variable keeps the number of cards in the deck and updates it when a
	// card is dealt. This variable is maintained so that the size of the deck
	// is not calculated in every function call.
	private int numberOfCardsInDeck = 0;

	// This is used to generate random indices in the deck
	private Random randomNumGenerator = new Random();

	// The following two static arrays are used to generate the deck in the
	// constructor. They represent the ranks and suits respectively.
	private static String[] cardRanks = { "A", "2", "3", "4", "5", "6", "7",
			"8", "9", "10", "J", "Q", "K" };
	private static String[] cardSuits = { "S", "D", "H", "C" };

	/**
	 * The default constructor populates the cards list with one card for each
	 * rank and suit
	 */
	public Deck() {
		for (String rank : cardRanks)
			for (String suit : cardSuits)
				cards.add(rank + " " + suit);

		numberOfCardsInDeck = cards.size();
	}

	/**
	 * This function shuffles the deck. The shuffle function repeatedly swaps
	 * two cards on randomly selected positions in the deck.
	 */
	public void shuffle() {
		/*
		 * The logic employed in this function is that of repeatedly swapping
		 * cards at two random positions in the deck, instead of a logic that
		 * would remove cards from certain positions and add them back to the
		 * list. The reason is that any operation that would remove cards from
		 * the list would be computationally more expensive.
		 */

		/*
		 * If there are fewer than two cards in the deck then shuffling is not
		 * possible
		 */
		if (numberOfCardsInDeck < 2)
			return;

		/*
		 * Generate a random number between half the deck size and the total
		 * deck size. This is the number of times two randomly chosen cards will
		 * be swapped.
		 */
		int numberOfSwaps = randomNumGenerator.nextInt(numberOfCardsInDeck / 2)
				+ numberOfCardsInDeck / 2;

		String temporaryCardHolder = null;
		int index1, index2;

		/*
		 * This loop repeatedly generates two random indices in the deck and
		 * swap the two cards on those indices
		 */
		for (int i = 0; i < numberOfSwaps; i++) {
			index1 = getRandomIndexInDeck();
			index2 = getRandomIndexInDeck();

			if (index1 == index2)
				continue;

			temporaryCardHolder = cards.get(index1);
			cards.set(index1, cards.get(index2));
			cards.set(index2, temporaryCardHolder);
		}

	}

	/**
	 * This function deals card from the top of the deck and return it
	 * 
	 * @return String representing a card dealt from the top of the deck. The
	 *         string is composed of rank and suit in order
	 * @throws Exception
	 *             An exception is thrown if the deck is empty
	 */
	public String deal() throws EmptyDeckException {
		if (numberOfCardsInDeck == 0)
			throw new EmptyDeckException();

		numberOfCardsInDeck--;
		return cards.remove(0);
	}

	/**
	 * Returns the current size of the deck
	 * 
	 * @return current deck size
	 */
	public int size() {
		return numberOfCardsInDeck;
	}

	/**
	 * This function generates a random number between 0 and size of the deck
	 * 
	 * @return random number between 0 and size of the deck
	 */
	private int getRandomIndexInDeck() {
		return randomNumGenerator.nextInt(numberOfCardsInDeck);
	}

}
