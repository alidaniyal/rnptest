package rnp.test.daniyal;

public class EmptyDeckException extends Exception{
	public EmptyDeckException(){
		super("The deck is empty!");
	}
	
	public String toString(){
		return super.toString();
	}
}
