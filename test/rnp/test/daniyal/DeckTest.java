package rnp.test.daniyal;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.Test;

/**
 * This class implements a number of tests to check the functionality of the
 * deck class.
 * 
 * @author ali.daniyal
 * 
 */
public class DeckTest {
	@Test
	/**
	 * Check if the deck of the size is 52 initially
	 */
	public void checkDeckSize() {
		Deck deck = new Deck();
		assertEquals(deck.size(), 52);
	}

	@Test
	/**
	 * Deal 10 cards and check that the size is 42 and then deal 42 more to 
	 * check that no more card should be left in the deck. This test checks
	 * if dealing cards changes the size of the deck correctly.
	 */
	public void dealAndCheckDeckSize() throws Exception {
		Deck deck = new Deck();
		for (int i = 0; i < 10; i++)
			deck.deal();
		assertEquals(deck.size(), 42);
		for (int i = 0; i < 42; i++)
			deck.deal();
		assertEquals(deck.size(), 0);
	}

	@Test(expected = EmptyDeckException.class)
	/**
	 * This test checks if exception is thrown when deal is performed on
	 * an empty deck. 52 cards are dealt and then another is dealt.
	 */
	public void checkDeckEmpty() throws Exception {
		Deck deck = new Deck();
		for (int i = 0; i < 53; i++)
			deck.deal();
	}

	@Test
	/**
	 * This test checks if dealt cards are unique to see if there is
	 * no mistake related to card repetition. Also after dealing each 
	 * card the size of the deck is also checked.
	 */
	public void checkUniqueDeals() throws Exception {
		ArrayList<String> cards = new ArrayList<String>();

		Deck deck = new Deck();
		/*
		 * Cards are dealt and added to an arraylist to check against the list
		 * that future cards are not already present in that list
		 */
		for (int i = 0; i < 52; i++) {
			String card = deck.deal();
			assertFalse(cards.contains(card));
			cards.add(card);
			assertEquals(cards.size(), i + 1);
		}
	}

	@Test
	/**
	 * This test checks that shuffling operations maintains the uniqueness
	 * of the dealt cards. So after every deal it shuffles the deck.
	 */
	public void checkUniqueDealsWithShuffle() throws Exception {
		ArrayList<String> cards = new ArrayList<String>();

		Deck deck = new Deck();
		for (int i = 0; i < 52; i++) {
			String card = deck.deal();
			deck.shuffle();
			assertFalse(cards.contains(card));
			cards.add(card);
			assertEquals(cards.size(), i + 1);
		}
	}

	@Test
	/**
	 * This test checks that each card dealt has a valid card name of the 
	 * format RANK\bSUIT (where \b stands for the space character).
	 */
	public void checkValidCards() throws Exception {
		Deck deck = new Deck();
		for (int i = 0; i < 52; i++) {
			String card = deck.deal();
			deck.shuffle();
			Pattern pattern = Pattern.compile("^(A|[2-9]|10|J|Q|K) (S|D|H|C)$");
			Matcher matcher = pattern.matcher(card);
			assertTrue(matcher.matches());
		}

		assertEquals(deck.size(), 0);
	}

	@Test
	/**
	 * This test checks that shuffle operation changes the order of the deck.
	 * Although this is not guaranteed, but if multiple executions of the test
	 * doesn't show change in order in shuffling then the shuffling operation
	 * is not implemented well.
	 */
	public void testShuffling() throws Exception {
		/*
		 * The function generates a long string by appending names of all the
		 * cards dealt in sequence from a deck. If this representation of two
		 * given decks is different from each other, then their order is
		 * different. This method is used to check if shuffled deck has a
		 * different order or not.
		 */

		String order1 = "";
		Deck deck = new Deck();
		// Generate the string representation for deck1
		for (int i = 0; i < 52; i++) {
			order1 += deck.deal();
		}

		String order2 = "";
		deck = new Deck();
		// Generate the string representation for deck2
		for (int i = 0; i < 52; i++) {
			order2 += deck.deal();
		}

		// Since none of the previous two decks were shuffled
		// the string representation must be the same
		assertEquals(order1, order2);
		assertTrue(order1.length() > 0);

		String order3 = "";
		deck = new Deck();
		deck.shuffle();
		// Generate the string representation of the shuffled deck
		for (int i = 0; i < 52; i++) {
			order3 += deck.deal();
		}
		// The string representation of the shuffled deck is highly likely to be
		// different from the deck generated first, but lengths must be the
		// same. Since it is not logically guaranteed that the shuffled deck
		// is different from the original deck, this test can fail at times.
		assertFalse(order1.equals(order3));
		assertEquals(order1.length(), order3.length());
	}
}
