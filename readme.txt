The repository contains a class that implements deck of playing cards that 
supports operations for shuffling the deck and drawing the cards from the 
deck. It also contains a JUnit test class for testing the functionality of the
Deck class.

Source is compatible with Java 1.5 and higher.

An ant script is also provided that compiles the classes and runs the tests
against the class.